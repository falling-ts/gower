import client from "../lib/client";

interface Data {
    id?: number
    username: string
    password: string
}

async function login(data: Data, option: RequestInit = {}) {
    return await client.post("/admin/auth/login", data, option)
}

async function logout(data: Data | object = {}, option: RequestInit = {}) {
    return await client.post("/admin/auth/logout", data, option)
}

async function image(data: FormData, option: RequestInit = {}) {
    return await client.post("/admin/upload/image", data, option)
}

async function adminStore(data: Data, option: RequestInit = {}) {
    return await client.post("/admin/system/admin", data, option)
}

async function adminUpdate(data: Data, option: RequestInit = {}) {
    return await client.put(`/admin/system/admin/${data.id}`, data, option)
}

async function adminDestroy(data: Data, option: RequestInit = {}) {
    return await client.del(`/admin/system/admin/${data.id}`, data, option)
}

async function roleStore(data: Data, option: RequestInit = {}) {
    return await client.post("/admin/system/role", data, option)
}

async function roleUpdate(data: Data, option: RequestInit = {}) {
    return await client.put(`/admin/system/role/${data.id}`, data, option)
}

async function roleDestroy(data: Data, option: RequestInit = {}) {
    return await client.del(`/admin/system/role/${data.id}`, data, option)
}

export default {
    login,
    logout,
    image,
    adminStore,
    adminUpdate,
    adminDestroy,
    roleStore,
    roleUpdate,
    roleDestroy
}
