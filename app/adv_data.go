package app

import (
	"errors"
	"reflect"
	"strings"
	"time"

	"gitee.com/falling-ts/gower/utils/slice"
	"gitee.com/falling-ts/gower/utils/str"
)

type Rule map[string]any
type Skips []string

var config = Config()

// Trans 高级数据转换
func Trans(src any, r Rule) (any, error) {
	srcValue := reflect.Indirect(reflect.ValueOf(src))
	if srcValue.Kind() == reflect.Array || srcValue.Kind() == reflect.Slice {
		dest := make([]any, srcValue.Len())
		for i := 0; i < srcValue.Len(); i++ {
			mapType := reflect.MapOf(reflect.TypeOf(""), reflect.TypeOf(new(any)).Elem())
			destValue := reflect.MakeMap(mapType)
			if err := trans(destValue, srcValue.Index(i), r); err != nil {
				return nil, err
			}
			dest[i] = destValue.Interface()
		}
		return dest, nil
	}

	mapType := reflect.MapOf(reflect.TypeOf(""), reflect.TypeOf(new(any)).Elem())
	destValue := reflect.MakeMap(mapType)
	if err := trans(destValue, srcValue, r); err != nil {
		return nil, err
	}
	return destValue.Interface(), nil
}

// TransTo 高级数据转换至目标
func TransTo(dest any, src any, r Rule) (any, error) {
	srcValue := reflect.Indirect(reflect.ValueOf(src))
	if srcValue.Kind() == reflect.Array || srcValue.Kind() == reflect.Slice {
		destSlice := make([]any, srcValue.Len())
		for i := 0; i < srcValue.Len(); i++ {
			destValue := reflect.Indirect(reflect.ValueOf(dest))
			if err := trans(destValue, srcValue.Index(i), r); err != nil {
				return nil, err
			}
			destSlice[i] = destValue.Interface()
		}
		return destSlice, nil
	}

	destValue := reflect.Indirect(reflect.ValueOf(dest))
	if err := trans(destValue, srcValue, r); err != nil {
		return nil, err
	}
	return dest, nil
}

func trans(dest reflect.Value, src reflect.Value, r map[string]any) error {
	dest = reflect.Indirect(dest)
	src = reflect.Indirect(src)

	s, ok := r["_skips"].([]string)
	if !ok {
		s, ok = r["_skips"].(Skips)
	}
	if ok {
		rawSkips := slice.Strings(s)
		skips := rawSkips.Map(func(s string) string {
			return str.Conv(s).UpCamel()
		})

		destType := dest.Type()
		switch destType.Kind() {
		case reflect.Struct:
			destNoSkips(destType, dest, src, skips, r)
		case reflect.Map:
			keys := dest.MapKeys()
			if len(keys) == 0 {
				switch src.Kind() {
				case reflect.Struct:
					srcNoSkips(src.Type(), src, skips, r)
				case reflect.Map:
					srcKeys := src.MapKeys()
					for _, key := range srcKeys {
						fieldName := key.String()
						if isContinue(fieldName, rawSkips, r) {
							continue
						}

						r[fieldName] = fieldName
					}
				default:
				}
			} else {
				for _, key := range keys {
					fieldName := key.String()
					if isContinue(fieldName, rawSkips, r) {
						continue
					}

					switch src.Kind() {
					case reflect.Struct:
						srcField := src.FieldByName(fieldName)
						if !srcField.IsValid() {
							continue
						}
					default:
					}

					r[fieldName] = fieldName
				}
			}
		default:
		}

		delete(r, "_skips")
	}

	var (
		destValue reflect.Value
		srcValue  reflect.Value
		argValue  reflect.Value
		err       error
	)
	for k, v := range r {
		destValue, err = valueByKey(dest, k)
		if err != nil {
			return err
		}

		rule := reflect.ValueOf(v)
		switch rule.Kind() {
		case reflect.Func:
			arg := make([]reflect.Value, 0)
			if rule.Type().NumIn() == 1 {
				arg = append(arg, src)
			}

			results := rule.Call(arg)
			for _, result := range results {
				res := result.Interface()
				if res == nil {
					continue
				}
				switch res.(type) {
				case error:
					return res.(error)
				default:
					setValue(dest, destValue, k, results[0])
				}
			}
		case reflect.Struct:
			argsValue := rule.FieldByName("Args")
			funcValue := rule.FieldByName("Func")
			if !argsValue.IsValid() {
				break
			}
			if !funcValue.IsValid() {
				break
			}

			var args []string
			args, ok = argsValue.Interface().([]string)
			if !ok {
				break
			}
			if funcValue.Kind() != reflect.Func {
				break
			}

			realArgs := make([]reflect.Value, len(args))
			for i, arg := range args {
				argValue, err = valueByKey(src, arg)
				if err != nil {
					realArgs[i] = argValue
					continue
				}
				argValue = reflect.ValueOf(arg)

				realArgs[i] = argValue
			}

			results := funcValue.Call(realArgs)
			for _, result := range results {
				res := result.Interface()
				if res == nil {
					continue
				}
				switch res.(type) {
				case error:
					return res.(error)
				default:
					setValue(dest, destValue, k, results[0])
				}
			}
		case reflect.Map:
			srcValue, err = valueByKey(src, k)
			if err != nil {
				setValue(dest, destValue, k, rule)
				break
			}

			destValue = reflect.Indirect(destValue)
			srcValue = reflect.Indirect(srcValue)

			switch destValue.Kind() {
			case reflect.Array, reflect.Slice:
				if srcValue.Kind() != reflect.Array && srcValue.Kind() != reflect.Slice {
					setValue(dest, destValue, k, rule)
					break
				}
				if srcValue.Len() == 0 {
					break
				}

				elemType := destValue.Type().Elem()
				if destValue.Kind() == reflect.Slice {
					destValue = reflect.MakeSlice(reflect.SliceOf(elemType), srcValue.Len(), srcValue.Len())
				}

				for i := 0; i < srcValue.Len(); i++ {
					switch elemType.Kind() {
					case reflect.Ptr:
						destValue.Index(i).Set(reflect.New(elemType))
					case reflect.Map:
						destValue.Index(i).Set(reflect.MakeMap(elemType))
					default:
						destValue.Index(i).Set(reflect.New(elemType).Elem())
					}

					_r, ok := v.(map[string]any)
					if !ok {
						_r, ok = v.(Rule)
					}
					if !ok {
						return errors.New("规则类型错误")
					}
					if err = trans(destValue.Index(i), srcValue.Index(i), _r); err != nil {
						return err
					}
				}
			case reflect.Map, reflect.Struct:
				_r, ok := v.(map[string]any)
				if !ok {
					_r, ok = v.(Rule)
				}
				if !ok {
					return errors.New("规则类型错误")
				}
				if err = trans(destValue, srcValue, _r); err != nil {
					return err
				}
			default:
				switch srcValue.Kind() {
				case reflect.Array, reflect.Slice:
					if srcValue.Len() == 0 {
						break
					}

					elemType := reflect.MapOf(reflect.TypeOf(""), reflect.TypeOf(new(any)).Elem())
					if srcValue.Kind() == reflect.Array {
						array := reflect.ArrayOf(srcValue.Len(), elemType)
						setValue(dest, destValue, k, reflect.New(array).Elem())
					} else {
						makeSlice := reflect.MakeSlice(reflect.SliceOf(elemType), srcValue.Len(), srcValue.Len())
						setValue(dest, destValue, k, makeSlice)
					}

					for i := 0; i < srcValue.Len(); i++ {
						destValue.Elem().Index(i).Set(reflect.MakeMap(elemType))

						_r, ok := v.(map[string]any)
						if !ok {
							_r, ok = v.(Rule)
						}
						if !ok {
							return errors.New("规则类型错误")
						}
						if err = trans(destValue.Elem().Index(i), srcValue.Index(i), _r); err != nil {
							return err
						}
					}
				case reflect.Map, reflect.Struct:
					mapType := reflect.MapOf(reflect.TypeOf(""), reflect.TypeOf(new(any)).Elem())
					data := reflect.MakeMap(mapType)
					setValue(dest, destValue, k, data)

					_r, ok := v.(map[string]any)
					if !ok {
						_r, ok = v.(Rule)
					}
					if !ok {
						return errors.New("规则类型错误")
					}
					if err = trans(destValue.Elem(), srcValue, _r); err != nil {
						return err
					}
				default:
				}
			}
		case reflect.String:
			vStr, def := getSrcKeyAndDefault(k, v.(string))
			srcValue, err = valueByKey(src, vStr)

			if err != nil ||
				!srcValue.IsValid() ||
				(srcValue.Kind() == reflect.String && srcValue.String() == "") ||
				(srcValue.Kind() == reflect.Ptr && srcValue.Elem().Kind() == reflect.String && srcValue.Elem().String() == "") {
				setValue(dest, destValue, k, def)
				break
			}

			if Format := srcValue.MethodByName("Format"); Format.IsValid() && srcValue.Kind() == reflect.Ptr && !srcValue.IsNil() {
				srcValue = Format.Call([]reflect.Value{
					reflect.ValueOf(time.DateTime),
				})[0]
			}
			setValue(dest, destValue, k, srcValue)
		default:
			setValue(dest, destValue, k, rule)
		}
	}

	return nil
}

func getSrcKeyAndDefault(descKey string, srcKey string) (string, reflect.Value) {
	if srcKey == "" {
		return descKey, reflect.ValueOf("")
	}

	parts := strings.Split(srcKey, "||")
	if parts[0] == "" {
		return descKey, reflect.ValueOf(parts[1])
	}
	if len(parts) > 1 {
		return parts[0], reflect.ValueOf(parts[1])
	}

	if descKey == srcKey {
		return descKey, reflect.ValueOf("")
	}

	return srcKey, reflect.ValueOf(srcKey)
}

func destNoSkips(destType reflect.Type, dest reflect.Value, src reflect.Value, skips slice.Strings, r Rule) {
	for i := 0; i < destType.NumField(); i++ {
		fieldType := destType.Field(i)
		fieldName := fieldType.Name
		field := dest.FieldByName(fieldName)
		typ := field.Type()

		if fieldType.Tag.Get("gorm") == "-" {
			continue
		}
		if fieldType.Anonymous {
			destNoSkips(typ, field, src, skips, r)
			continue
		}
		if isContinue(fieldName, skips, r) {
			continue
		}

		switch src.Kind() {
		case reflect.Struct:
			srcField := src.FieldByName(fieldName)
			if !srcField.IsValid() {
				continue
			}

			fieldElem := field.Type()
			if field.Kind() == reflect.Ptr {
				fieldElem = field.Type().Elem()
			}

			srcFieldElem := srcField.Type()
			if srcField.Kind() == reflect.Ptr {
				srcFieldElem = srcField.Type().Elem()
			}

			if fieldElem.Kind() != srcFieldElem.Kind() {
				continue
			}
		case reflect.Map:
			convFieldName := str.Conv(fieldName)
			result := src.MapIndex(reflect.ValueOf(fieldName))
			if result.IsValid() {
				r[fieldName] = fieldName
				continue
			}
			result = src.MapIndex(reflect.ValueOf(convFieldName.Camel()))
			if result.IsValid() {
				r[fieldName] = fieldName
				continue
			}
			result = src.MapIndex(reflect.ValueOf(convFieldName.Snake()))
			if result.IsValid() {
				r[fieldName] = fieldName
				continue
			}

			continue
		default:
		}

		r[fieldName] = fieldName
	}
}

func srcNoSkips(srcType reflect.Type, src reflect.Value, skips slice.Strings, r Rule) {
	for i := 0; i < srcType.NumField(); i++ {
		fieldType := srcType.Field(i)
		fieldName := fieldType.Name
		field := src.FieldByName(fieldName)
		typ := field.Type()

		if fieldType.Tag.Get("gorm") == "-" {
			continue
		}
		if fieldType.Anonymous {
			srcNoSkips(typ, field, skips, r)
			continue
		}
		if isContinue(fieldName, skips, r) {
			continue
		}

		r[fieldName] = fieldName
	}
}

func isContinue(fieldName string, skips slice.Strings, r Rule) bool {
	var ok bool
	convFieldName := str.Conv(fieldName)

	if skips.Has(fieldName) {
		return true
	}
	if _, ok = r[fieldName]; ok {
		return true
	}
	if _, ok = r[convFieldName.Camel()]; ok {
		return true
	}
	if _, ok = r[convFieldName.Snake()]; ok {
		return true
	}

	return false
}

func valueByKey(v reflect.Value, k string) (reflect.Value, error) {
	var result reflect.Value
	k = str.Conv(k).UpCamel()

	switch v.Kind() {
	case reflect.Map:
		convK := str.Conv(k)
		result = v.MapIndex(reflect.ValueOf(k))
		if result.IsValid() {
			break
		}
		result = v.MapIndex(reflect.ValueOf(convK.Camel()))
		if result.IsValid() {
			break
		}
		result = v.MapIndex(reflect.ValueOf(convK.Snake()))
		if result.IsValid() {
			break
		}
		result = reflect.ValueOf(new(any)).Elem()
	case reflect.Struct:
		result = v.FieldByName(k)
	default:
	}
	if result.IsValid() {
		return result, nil
	}

	return result, errors.New("类型错误")
}

func setValue(dest reflect.Value, destValue reflect.Value, k string, v reflect.Value) {
	k = str.Conv(k).UpCamel()

	switch dest.Kind() {
	case reflect.Map:
		destValue.Set(v)
		switch config.Res.KeyType {
		case "CamelType":
			dest.SetMapIndex(reflect.ValueOf(k), destValue)
		case "camelType":
			dest.SetMapIndex(reflect.ValueOf(str.Conv(k).Camel()), destValue)
		default:
			dest.SetMapIndex(reflect.ValueOf(str.Conv(k).Snake()), destValue)
		}
	case reflect.Struct:
		if destValue.Kind() == reflect.Ptr && v.Kind() != reflect.Ptr {
			if v.CanAddr() {
				destValue.Set(v.Addr())
			} else {
				newV := reflect.New(v.Type()).Elem()
				newV.Set(v)
				destValue.Set(newV.Addr())
			}

		} else if destValue.Kind() != reflect.Ptr && v.Kind() == reflect.Ptr {
			destValue.Set(v.Elem())
		} else {
			destValue.Set(v)
		}
	default:
	}
}
