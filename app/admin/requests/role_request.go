package requests

import "gitee.com/falling-ts/gower/app"

type RoleRequest struct {
	app.Request
}

type RoleIndexRequest struct {
	app.IndexRequest
	Name string `form:"name" json:"name" zh:"角色名称"`
	Flag string `form:"flag" json:"flag" zh:"角色标记"`
}

type RoleCreateEditRequest struct {
	app.ModalRequest
	FormType string `form:"formType" json:"formType" zh:"表单类型"`
}

type RoleStoreRequest struct {
	RoleRequest
	Name string `form:"name" json:"name" binding:"required" zh:"角色名称"`
	Flag string `form:"flag" json:"flag" binding:"required" zh:"角色标记"`
}

type RoleUpdateRequest struct {
	RoleRequest
	Name string `form:"name" json:"name" zh:"角色名称"`
	Flag string `form:"flag" json:"flag" zh:"角色标记"`
}
