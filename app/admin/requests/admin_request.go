package requests

import "gitee.com/falling-ts/gower/app"

type AdminRequest struct {
	app.Request
	Email    string `form:"email" json:"email" zh:"邮箱"`
	Nickname string `form:"nickname" json:"nickname" zh:"昵称"`
	Avatar   string `form:"avatar" json:"avatar"  zh:"头像"`
}

type AdminIndexRequest struct {
	app.IndexRequest
	Username string `form:"username" json:"username" zh:"用户名"`
	Email    string `form:"email" json:"email" zh:"邮箱"`
	Nickname string `form:"nickname" json:"nickname" zh:"昵称"`
}

type AdminCreateEditRequest struct {
	app.ModalRequest
	FormType string `form:"formType" json:"formType" zh:"表单类型"`
}

type AdminStoreRequest struct {
	AdminRequest
	Username string `form:"username" json:"username" binding:"required" zh:"用户名"`
	Password string `form:"password" json:"password" binding:"required" zh:"密码"`
}

type AdminUpdateRequest struct {
	AdminRequest
	Username string `form:"username" json:"username" zh:"用户名"`
	Password string `form:"password" json:"password" zh:"密码"`
}
