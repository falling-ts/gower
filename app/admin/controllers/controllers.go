package controllers

import "gitee.com/falling-ts/gower/app"

var (
	trans  = app.Translate()
	exc    = app.Exception()
	passwd = app.Passwd()
	res    = app.Response()
	config = app.Config()
	auth   = app.Auth()
	cookie = app.Cookie()
	db     = app.DB()
	upload = app.Upload()
	util   = app.Util()
)
