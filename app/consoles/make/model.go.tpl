package models

import (
	"{{.Module}}/app"
)

func init() {
	migrate(new({{.UpCamel}}))
}

type {{.UpCamel}} struct {
	Model

	// Name *string `gorm:"type:string;default:'';comment:名称"`
}

// In 高级数据填充
func ({{.FirstChar}} *{{.UpCamel}}) In(src any, r app.Rule) (any, error) {
	return app.TransTo({{.FirstChar}}, src, r)
}

// Out 高级数据导出
func ({{.FirstChar}} *{{.UpCamel}}) Out(r app.Rule) (any, error) {
	return app.Trans({{.FirstChar}}, r)
}
