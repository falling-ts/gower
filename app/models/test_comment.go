package models

import "gitee.com/falling-ts/gower/app"

func init() {
	migrate(new(TestComment))
}

type TestComment struct {
	Model
	Content   *string `gorm:"type:text;comment:评论内容"`
	UserID    *uint
	User      *TestUser `gorm:"foreignKey:UserID"`
	ArticleID *uint
	Article   *TestArticle `gorm:"foreignKey:ArticleID"`
	ParentID  *uint
	Parent    *TestComment  `gorm:"foreignKey:ParentID"`
	Children  []TestComment `gorm:"foreignKey:ParentID"`
}

// In 高级数据填充
func (t *TestComment) In(src any, r app.Rule) (any, error) {
	return app.TransTo(t, src, r)
}

// Out 高级数据导出
func (t *TestComment) Out(r app.Rule) (any, error) {
	return app.Trans(t, r)
}
