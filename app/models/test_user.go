package models

import "gitee.com/falling-ts/gower/app"

func init() {
	migrate(new(TestUser))
}

type TestUser struct {
	Model
	Username   *string        `gorm:"type:string;uniqueIndex;comment:用户名"`
	Password   string         `gorm:"type:string;comment:密码"`
	Email      *string        `gorm:"type:string;uniqueIndex;comment:邮箱"`
	UserInfo   TestUserInfo   `gorm:"foreignKey:UserID"`
	Categories []TestCategory `gorm:"foreignKey:UserID"`
	Articles   []TestArticle  `gorm:"foreignKey:UserID"`
	Comments   []TestComment  `gorm:"foreignKey:UserID"`
}

// In 高级数据填充
func (t *TestUser) In(src any, r app.Rule) (any, error) {
	return app.TransTo(t, src, r)
}

// Out 高级数据导出
func (t *TestUser) Out(r app.Rule) (any, error) {
	return app.Trans(t, r)
}
