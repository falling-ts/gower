package models

import "gitee.com/falling-ts/gower/app"

func init() {
	migrate(new(TestUserInfo))
}

type TestUserInfo struct {
	Model
	Nickname *string `gorm:"type:string;default:'';comment:昵称"`
	Avatar   *string `gorm:"type:string;default:'';comment:头像"`
	UserID   *uint
	User     *TestUser `gorm:"foreignKey:UserID"`
}

// In 高级数据填充
func (t *TestUserInfo) In(src any, r app.Rule) (any, error) {
	return app.TransTo(t, src, r)
}

// Out 高级数据导出
func (t *TestUserInfo) Out(r app.Rule) (any, error) {
	return app.Trans(t, r)
}
