package models

import "gitee.com/falling-ts/gower/app"

func init() {
	migrate(new(AdminRole))
}

type AdminRole struct {
	Model

	Name *string `gorm:"type:string;default:'';comment:角色名称"`
	Flag *string `gorm:"type:string;default:'';comment:角色标记"`

	Users []*AdminUser `gorm:"many2many:admin_role_users;joinForeignKey:role_id;joinReferences:user_id"`

	Menus []*AdminMenu `gorm:"many2many:admin_menu_roles;joinForeignKey:role_id;joinReferences:menu_id"`

	Permissions []*AdminPermission `gorm:"many2many:admin_permission_roles;joinForeignKey:role_id;joinReferences:permission_id"`
}

// In 高级数据填充
func (a *AdminRole) In(src any, r app.Rule) (any, error) {
	return app.TransTo(a, src, r)
}

// Out 高级数据导出
func (a *AdminRole) Out(r app.Rule) (any, error) {
	return app.Trans(a, r)
}
