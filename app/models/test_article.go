package models

import "gitee.com/falling-ts/gower/app"

func init() {
	migrate(new(TestArticle))
}

type TestArticle struct {
	Model
	Title      string  `gorm:"type:string;default:'';not null;commit:标题"`
	Content    *string `gorm:"type:text;commit:内容"`
	CategoryID *uint
	Category   *TestCategory `gorm:"foreignKey:CategoryID"`
	UserID     *uint
	User       *TestUser     `gorm:"foreignKey:UserID"`
	Comments   []TestComment `gorm:"foreignKey:ArticleID"`
}

// In 高级数据填充
func (t *TestArticle) In(src any, r app.Rule) (any, error) {
	return app.TransTo(t, src, r)
}

// Out 高级数据导出
func (t *TestArticle) Out(r app.Rule) (any, error) {
	return app.Trans(t, r)
}
