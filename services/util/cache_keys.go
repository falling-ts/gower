package util

import "strings"

// ResKey 响应缓存 key
func (s *Service) ResKey() string {
	return strings.Join([]string{"res", s.Nanoid()}, " ")
}

// ExcKey 异常缓存 key
func (s *Service) ExcKey() string {
	return strings.Join([]string{"exc", s.Nanoid()}, " ")
}

// BlackTokenKey Token 黑名单缓存 key
func (s *Service) BlackTokenKey(nanoid string) string {
	return strings.Join([]string{"black-token", nanoid}, " ")
}
